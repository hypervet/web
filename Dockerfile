FROM node:8.15.0-alpine as frontend
WORKDIR /assets
COPY assets/.npmrc assets/package.json assets/.babelrc assets/webpack.config.js /assets/
COPY assets .
RUN npm install
RUN npm rebuild node-sass
RUN npm run build

FROM python:3.7 as backend
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE "hypervet.settings.docker"
RUN apt-get update
RUN apt-get install -y build-essential
RUN apt-get install -y cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
WORKDIR /code/
RUN pip install pipenv
COPY .env Pipfile Pipfile.lock entrypoint.dev.sh entrypoint.prod.sh /code/
COPY --from=frontend /assets/ /assets/
RUN pipenv install
COPY hypervet /code/

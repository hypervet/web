#!/bin/sh

cd hypervet/
pipenv run ./manage.py migrate
pipenv run ./manage.py collectstatic -l --no-input
pipenv run ./manage.py runserver 0.0.0.0:8000
#pipenv run gunicorn hypervet.wsgi
exec "$@"

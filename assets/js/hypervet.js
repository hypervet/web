function setPictureAsBackground(element, backgroundPos, image_url) {
  /* set the picture as background on the provided element */
  element.css('background-image', 'url(' + image_url + ')');
  element.css('background-position', backgroundPos);
}

function getImages() {
  $('.carousel--slide').each(function (index) {
    var backgroundSet = false;
    var $slide = $(this);
    var $picture = $(this).find('picture');
    var backgroundPos = $picture.data('background-position');
    $($picture).children('source').each(function (index) {
      console.log($(this).attr('srcset'));
      if ($(this).data('max-width') > screen.width) {
        if ($(this).data('portrait') == true) {
          if (window.matchMedia("(orientation:portrait)").matches) {
            setPictureAsBackground(
              $slide, backgroundPos, $(this).attr('srcset'));
            backgroundSet = true;
            return false;
          }
        } else if ($(this).data('portrait') === undefined) {
          setPictureAsBackground(
            $slide, backgroundPos, $(this).attr('srcset'));
          backgroundSet = true;
          return false;
        }
      }
    });
    if(!backgroundSet) {
      // Go for ultra-wide image specified in img
      setPictureAsBackground(
        $slide, backgroundPos, $picture.find('img').attr('src'));
    }
    $picture.find('.carousel--img').css('visibility', 'hidden');
    $picture.find('.carousel--img').css('display', 'none');
  });
}

$(window).resize(function() {
  getImages()
});

getImages();

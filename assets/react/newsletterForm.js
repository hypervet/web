import React, {Component} from 'react';
import ReactDOM from 'react-dom';

class NewsletterForm extends Component {
    state = {
      email: ''
    }
    handleChange = event => {
      this.setState({email: event.target.value})
    }

    handleSubmit = (event) => {
      event.preventDefault();
      const params = {
        email: this.state.email,
        list: 'footer',
      }
      this.setState({email: ''})
      let url = new URL(SUBSCRIBE_URL);
      Object
        .keys(params)
        .forEach(key => url.searchParams.append(key, params[key]))

      fetch(url);
    }

    render() {
      return (
        <form onSubmit={this.handleSubmit}>
          <input
            type="email"
            className="form-control"
            value={this.state.email}
            placeholder="Your email address"
            onChange={this.handleChange} />
            <button type="submit">
              <i className="fas fa-angle-right"></i>
            </button>
          </form>
      );
    }
}


ReactDOM.render(<NewsletterForm />, window.newsLetterMount);

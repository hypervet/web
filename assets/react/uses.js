import React from 'react';
import ReactDOM from 'react-dom';

const Usage = props => (
  <div className="post-box uses">
    <div className="post-box-img">
      <img src={props.image.src} />
      <div className="post-box-content">
        <a href={window.location.href + props.slug}>
          <h5>{props.title}</h5>
        </a>
      </div>
    </div>
  </div>
);

const Uses = ({children}) => {
  const parsedChildren = JSON.parse(children);
  const usages = parsedChildren.map(child => (
    <li key={child.slug}>
      <Usage
        title={child.title}
        image={child.image}
        slug={child.slug}
      />
    </li>
  ));
  return usages;
}


ReactDOM.render(
  React.createElement(Uses, window.props),
  window.uses_mount,
);
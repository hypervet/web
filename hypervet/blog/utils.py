"""Utils for the blog system"""
import math

from bs4 import BeautifulSoup

AWM = 235  # Set average words read per minute

def calculate_reading_time(text):
    """Determine the average reading time for the given text."""
    reading_time = len(text.split()) / AWM
    return 1 if math.floor(reading_time) < 1 else math.floor(reading_time)

def cleanhtml(raw_html):
    """Remove html tags from text"""
    return BeautifulSoup(raw_html, "html.parser").text

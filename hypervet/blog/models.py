"""Models for the blog logic"""
from datetime import datetime, timedelta

from django.conf import settings
from django.core.paginator import Paginator
from django.db import models
from django.db.models import Count
from django.shortcuts import render
from django.utils.translation import get_language_from_path
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey
from taggit.models import Tag, TaggedItemBase
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.core.fields import RichTextField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index

from home.models import BasePage
from social.facebook import create_facebook_share_url
from social.linkedin import create_linkedin_share_url
from social.twitter import create_twitter_share_url
from .utils import calculate_reading_time, cleanhtml


class PostMetrics(models.Model):
    """Store metrics per Page"""
    page = models.ForeignKey('blog.BlogPost', on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)


class BlogPostTag(TaggedItemBase):
    """Tags for the blog"""
    content_object = ParentalKey(
        'blog.BlogPost', on_delete=models.CASCADE, related_name='tagged_items')


class BlogBasePage(BasePage):
    """Base for the blog views"""

    class Meta:
        abstract = True

    def get_context(self, request):
        context = super().get_context(request)
        context['tags'] = Tag.objects.all().order_by('name')
        context['popular_posts'] = self.get_popular_articles_for_session(
            request, limit=5)
        return context

    def get_popular_articles_for_session(self, request, limit=10):
        """Find the blog posts that are more relevant to the user"""
        language = get_language_from_path(request.path_info)
        time_treshold = (
            datetime.now() - timedelta(hours=settings.POPULAR_POST_HOUR_LIMIT))
        all_posts = BlogPost.objects.filter(language__code=language).live()
        all_posts = all_posts.annotate(num_views=Count('postmetrics'))
        eligible_posts = all_posts.filter(
            postmetrics__timestamp__gte=time_treshold)
        if len(eligible_posts) < limit:
            extra_posts = all_posts.exclude(
                id__in=eligible_posts.values_list('id', flat=True))
            eligible_posts = eligible_posts.union(extra_posts, all=False)
        return eligible_posts.order_by('-num_views')[:limit]


class BlogIndexPage(BlogBasePage):
    """Index page for the blog system"""
    banner_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='+'
    )

    content_panels = Page.content_panels + [
        ImageChooserPanel('banner_image'),
    ]

    subpage_types = ['blog.BlogPost']

    def serve(self, request, *args, **kwargs):
        """Serve the page

        Limit amount of blog posts to 10.
        On the first page show 11 posts, of which the first one is featured.
        """
        request.is_preview = getattr(request, 'is_preview', False)
        category = request.GET.get('category')
        order = '-first_published_at'
        homepage = self.get_parent()
        blog_index = self
        blog_entries = BlogPost.objects.child_of(self).live().order_by(order)
        if category:
            blog_entries = blog_entries.filter(tags__name=category)
            most_recent_entry = None
        else:
            try:
                most_recent_entry = blog_entries[0]
                blog_entries = blog_entries.exclude(id=most_recent_entry.id)
            except IndexError:
                most_recent_entry = None
        paginated_blog_entries = Paginator(blog_entries, 10)
        page_number = request.GET.get('page', 1)
        entries = paginated_blog_entries.page(page_number)
        featured_post = most_recent_entry if int(page_number) == 1 else None

        context = self.get_context(request, *args, **kwargs)
        context['page'] = self
        context['blog_index'] = blog_index
        context['blogposts'] = entries
        context['paginator'] = paginated_blog_entries
        context['featured_post'] = featured_post
        context['category'] = category

        return render(request, self.template, context)


class BlogPost(BlogBasePage):
    """Detailed blog post"""
    intro = models.CharField(max_length=250)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='+'
    )
    body = RichTextField(features=[
        'h2', 'h3', 'h4', 'link', 'ol', 'ul', 'image', 'embed', 'blockquote'])
    tags = ClusterTaggableManager(through=BlogPostTag, blank=True)

    search_fields = Page.search_fields + [
        index.SearchField('title'),
        index.SearchField('intro'),
        index.SearchField('body'),
        index.SearchField('tags'),
    ]

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            ImageChooserPanel('image'),
            FieldPanel('intro'),
            FieldPanel('body'),
            FieldPanel('tags'),
        ], heading='content')
    ]

    show_in_menus_default = False
    parent_pages_types = ['blog.BlogIndexPage']

    def serve(self, request, *args, **kwargs):
        """Serve the page, add extra info"""
        request.is_preview = getattr(request, 'is_preview', False)
        self.set_blog_views(request)
        self.update_metrics(request)
        blog_index = self.get_parent()

        context = self.get_context(request, *args, **kwargs)
        context['page'] = self
        context['blog_index'] = blog_index
        context['previous_post'] = self.get_prev_siblings().live().first()
        context['next_post'] = self.get_next_siblings().live().first()
        context['twitter_share_url'] = create_twitter_share_url(self)
        context['linkedin_share_url'] = create_linkedin_share_url(self)
        context['facebook_share_url'] = create_facebook_share_url(self)
        context['reading_time'] = calculate_reading_time(cleanhtml(self.body))
        context['related_posts'] = self.get_related_posts()

        return render(request, self.template, context)

    def get_related_posts(self, limit=5):
        """Get posts related to the requested one"""
        tags = self.tags.all()
        all_posts = BlogPost.objects.filter(language=self.language).live()
        if tags:
            posts = all_posts.filter(tags__in=tags)
        else:
            posts = all_posts
        return posts[:limit]


    def set_blog_views(self, request):
        """Keep track of what tags are popular for the user"""
        tags_stats = request.session.get('tags_stats')
        if not tags_stats:
            tags_stats = [
                {'id': tag.id, 'view_count': 1} for tag in self.tags.all()]
        else:
            tag_ids = [tag.id for tag in self.tags.all()]
            for tag_stat in tags_stats:
                if tag_stat['id'] in tag_ids:
                    tag_stat['view_count'] += 1
        request.session['tags_stats'] = tags_stats

    def update_metrics(self, request):
        """Keep record of page being shown"""
        if not request.is_preview:
            PostMetrics(page=self).save()

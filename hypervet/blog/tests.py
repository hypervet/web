import os

from .utils import calculate_reading_time, cleanhtml


def test_calculate_reading_time_minimum_one_minute():
    text = "I'm too short. My reading time should be one minute."
    assert calculate_reading_time(text) == 1


def test_calculate_reading_time_rounds_down():
    fixture_path = os.path.dirname(os.path.abspath(__file__))
    filename = "text_650_words.txt"
    with open(os.path.join(fixture_path, 'fixtures', filename)) as f:
        assert calculate_reading_time(f.read()) == 2


def test_cleanhtml_removes_tags():
    text = '<h1>Hi!</h1>'
    assert cleanhtml(text) == 'Hi!'


def test_cleanhtml_keeps_signs():
    text = '<p>Jonas <3 Kim</p>'
    assert cleanhtml(text) == 'Jonas <3 Kim'

def create_linkedin_share_url(page):
    """Create the LinkedIn share url for the provided page.

    Args:
        page: A Wagtail Page object

    Returns:
        str: a formatted url.

    Example:
        https://www.linkedin.com/sharing/share-offsite/?url=https://url.be
    """
    base_url = "https://www.linkedin.com/sharing/share-offsite/?url={}"
    return base_url.format(page.full_url)

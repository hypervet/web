def create_twitter_share_url(page):
    """Create the twitter share url for the provided page

    Args:
        page: A Wagtail Page object

    Returns:
        str: a formatted url.

    Example:
        https://twitter.com/intent/tweet?text=the%20text&url=https://url.be&via=HypervetCo
    """
    base_url = (
        "https://twitter.com/intent/tweet?text={text}&url={url}&via={via}")
    data = {
        "text": page.title,
        "url": page.full_url,
        "via": 'HypervetCo'
    }
    return base_url.format(**data)

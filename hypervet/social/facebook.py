"""Facebook helpers"""
from django.conf import settings
import requests

def create_facebook_share_url(page):
    """Create the Facebook share url for the provided page.

    Args:
        page: A Wagtail Page object

    Returns:
        str: a formatted url.

    Example:
        https://www.facebook.com/sharer.php?u=https://url.be&display=popup
    """
    base_url = "https://www.facebook.com/sharer.php?u={}&display=popup"
    return base_url.format(page.full_url)


def scrape_page(page):
    """Let the Facebook API scrape the provided page
    Args:
        page: A Wagtail Page object
    Returns:
        Nothing
    """
    base_url = "https://graph.facebook.com/v3.2/?scrape=true&id={}"
    params = {
        'scrape': True,
        'id': page.full_url,
        'access_token': settings.FACEBOOK_ACCESS_TOKEN
    }
    requests.post(base_url, **params)

from django.conf import settings
import requests


def add_subscriber(email, target_list):
    """Add a new email address to a Mailchimp list

    Args:
        email (unicode): The email address to add.
        target_list (unicode): The identifier of the Mailchimp list
    Returns:
        ...
    """
    mailchimp_endpoint = f'/lists/{target_list}/members/'
    requests.post(
        settings.MAILCHIMP_BASE_API_URL + mailchimp_endpoint,
        headers={
            'Authorization': 'apikey {}'.format(settings.MAILCHIMP_API_KEY)
        },
        json={
            "email_address": email,
            "status": "subscribed"
        }
    )

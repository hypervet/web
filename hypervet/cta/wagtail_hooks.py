from wagtail.contrib.modeladmin.options import (
    ModelAdmin, modeladmin_register)

from .models import EndOfPageCTA


class EndOfPageCTAModelAdmin(ModelAdmin):
    model = EndOfPageCTA
    menu_label = 'CTA blocks'
    menu_icon = 'folder-open-inverse'
    menu_order = 300
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ('title', 'live')
    search_fields = ('title',)
    list_filter = ('live',)


modeladmin_register(EndOfPageCTAModelAdmin)

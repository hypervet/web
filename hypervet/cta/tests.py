from unittest.mock import patch
from urllib.parse import urlencode

from django.http.response import Http404
from django.test import Client, override_settings
import pytest
from pytest import mark
from wagtail.tests.utils import WagtailTestUtils
import requests_mock

from .utils import add_subscriber

client = Client()
SUBSCRIBE_ENDPOINT = '/api/v1/subscribe/'


@mark.django_db
@patch('cta.views.add_subscriber')
def test_subscribe_endpoint_works(add_subscriber_mock):
    response = client.get(
        f'{SUBSCRIBE_ENDPOINT}?email=jonas@ghyllebert.be&list=footer')
    assert response.status_code == 200


@mark.django_db
@mark.parametrize("params", [
    {'list': 'footer'},
    {'email': 'jonas@ghyllebert.be'}
])
def test_subscribe_endpoint_fails_when_missing_parameter(params):
    qs = urlencode(params)
    response = client.get(f'{SUBSCRIBE_ENDPOINT}?{qs}')
    assert response.status_code == 404


@mark.django_db
def test_subscribe_endpoint_fails_when_incorrect_list():
    response = client.get(
        f'{SUBSCRIBE_ENDPOINT}?email=jonas@ghyllebert.be&list=list')
    assert response.status_code == 404


@mark.django_db
@patch('cta.views.add_subscriber')
def test_subscribe_endpoint_calls_add_subscriber(add_subscriber_mock):
    client.get(f'{SUBSCRIBE_ENDPOINT}?email=jonas@ghyllebert.be&list=footer')

    assert add_subscriber_mock.call_args[0][0] == 'jonas@ghyllebert.be'
    assert add_subscriber_mock.call_args[0][1] == '332325e58d'


@mark.django_db
@override_settings(MAILCHIMP_API_KEY='1234567890-eu8')
def test_add_subscriber_calls_mailchimp_endpoint():
    with requests_mock.Mocker() as r_mock:
        r_mock.post(
            'https://us19.api.mailchimp.com/3.0/lists/332325e58d/members/',
            request_headers={'Authorization': 'apikey 1234567890-eu8'},
            json={'status': 'subscribed'},
            status_code=200
        )
        client.get(
            f'{SUBSCRIBE_ENDPOINT}?email=jonas@ghyllebert.be&list=footer')

    assert r_mock.called

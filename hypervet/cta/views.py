from django.conf import settings
from django.http import Http404, JsonResponse
from django.views.generic import View

from .utils import add_subscriber


class SubscribeView(View):
    """Handle new subscriptions"""

    def __init__(self):
        self.lists = {
            'FOOTER': '332325e58d'
        }

    def get(self, request):
        """Process subscriptions

        Example:
            /api/v1/subscribe?list=footer&email_address=jonas@ghyllebert.be

        Args:
            request (dict): a dictionary containing the request data.

        Returns:
            200 if user got subscribed
            404 if user already on list
        """
        email = request.GET.get('email', '').lower()
        mailchimp_list = request.GET.get('list')
        if not all([email, mailchimp_list]):
            error_msg = (
                'Missing one or more parameters. Expected parameters: '
                '[email, list]')
            return JsonResponse({'error': error_msg}, status=404)

        target_list = self.lists.get(mailchimp_list.upper())
        if not target_list:
            return JsonResponse({'error': 'Invalid list'}, status=404)

        add_subscriber(email, target_list)

        return JsonResponse({'result': 'OK'})

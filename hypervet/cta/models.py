from django.db import models
from wagtail.admin.edit_handlers import (
    FieldPanel, MultiFieldPanel)
from wagtail.core.fields import RichTextField
from wagtail.images.edit_handlers import ImageChooserPanel


class EndOfPageCTA(models.Model):
    """Call to action to be shown at the end of a page."""

    title = models.CharField(max_length=128)
    description = RichTextField(null=True, blank=True)
    background_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='+'
    )
    button_text = models.CharField(max_length=50)
    live = models.NullBooleanField()

    panels = [
        MultiFieldPanel([
            FieldPanel('title'),
            FieldPanel('description')
        ], heading='content'),
        ImageChooserPanel('background_image'),
        FieldPanel('button_text'),
        FieldPanel('live')
    ]

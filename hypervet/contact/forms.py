from django import forms


class ContactForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.visible_fields():
            field.field.widget.attrs['class'] = 'form-control'
            # Prevent Lastpass from prefilling the fields
            field.field.widget.attrs['data-lpignore'] = 'true'

    name = forms.CharField(label='Name', required=True, max_length=100,)
    email = forms.EmailField(label='Email', required=True)
    subject = forms.CharField(label='Subject', required=False, max_length=100)
    message = forms.CharField(
        label='Message', required=True, widget=forms.Textarea)

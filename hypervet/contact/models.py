from django.db import models
from django.shortcuts import render

from wagtail.admin.edit_handlers import FieldPanel
from wagtail.core.fields import RichTextField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel

from home.models import BasePage
from .forms import ContactForm

class ContactPage(BasePage):
    banner_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='+'
    )
    intro = RichTextField()
    thank_you_text = RichTextField()

    content_panels = Page.content_panels + [
        ImageChooserPanel('banner_image'),
        FieldPanel('intro'),
        FieldPanel('thank_you_text'),
    ]

    def serve(self, request):
        context = self.get_context(request)
        if request.method == 'POST':
            form = ContactForm(request.POST)
            if form.is_valid():
                context['success'] = True
        else:
            form = ContactForm()
        context['form'] = form
        return render(request, 'contact/contact_page.html', context)

from .base import *  # NOQA

DEBUG = True
SECRET_KEY = os.environ['SECRET_KEY']
ALLOWED_HOSTS = ['hypervet.co', 'localhost']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': os.environ['POSTGRES_PASSWORD'],
        'HOST': 'db',
    }
}

STATICFILES_DIRS = STATICFILES_DIRS + [
    '/assets/dist/'
]

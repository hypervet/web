import logging

from django.conf import settings
from wagtail.core import hooks

from social.facebook import scrape_page

logger = logging.getLogger(__name__)


@hooks.register('after_create_page')
def do_facebook_page_scraper(request, page):
    """Call the facebook api to scrape the page.

    Facebook should only scrape the page if the page is live and got published.

    Args:
        request: Necessary to make the hook work
        page: a Wagtail Page object that just got created.

    Returns:
        None
    """
    if page.live:
        if settings.DEBUG:
            logger.info('Scrape page')
        else:
            scrape_page(page)

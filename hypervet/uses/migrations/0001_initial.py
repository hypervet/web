# Generated by Django 2.1.4 on 2018-12-19 17:57

from django.db import migrations, models
import django.db.models.deletion
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('wagtailimages', '0021_image_file_hash'),
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UsesPage',
            fields=[
                ('basepage_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='home.BasePage')),
                ('body', wagtail.core.fields.StreamField([('description', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock()), ('intro', wagtail.core.blocks.CharBlock(required=False)), ('text', wagtail.core.blocks.RichTextBlock()), ('image', wagtail.images.blocks.ImageChooserBlock()), ('image_position', wagtail.core.blocks.ChoiceBlock(choices=[('left', 'left'), ('right', 'right')])), ('page', wagtail.core.blocks.PageChooserBlock(required=False)), ('button_text', wagtail.core.blocks.CharBlock())])), ('cta', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock()), ('page', wagtail.core.blocks.PageChooserBlock()), ('button_text', wagtail.core.blocks.CharBlock())])), ('recent_blog_posts', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock()), ('intro', wagtail.core.blocks.RichTextBlock(required=False)), ('button_text', wagtail.core.blocks.CharBlock())])), ('testimonials', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock()), ('intro', wagtail.core.blocks.RichTextBlock(required=False)), ('testimonials', wagtail.core.blocks.ListBlock(wagtail.core.blocks.StructBlock([('text', wagtail.core.blocks.CharBlock()), ('author', wagtail.core.blocks.CharBlock()), ('workplace', wagtail.core.blocks.CharBlock())])))])), ('features', wagtail.core.blocks.StructBlock([('super_title', wagtail.core.blocks.CharBlock()), ('title', wagtail.core.blocks.CharBlock()), ('intro', wagtail.core.blocks.RichTextBlock(required=False)), ('features', wagtail.core.blocks.ListBlock(wagtail.core.blocks.StructBlock([('feature_name', wagtail.core.blocks.CharBlock()), ('text', wagtail.core.blocks.RichTextBlock(required=False)), ('image', wagtail.images.blocks.ImageChooserBlock()), ('page', wagtail.core.blocks.PageChooserBlock(required=False)), ('button_text', wagtail.core.blocks.CharBlock()), ('specifications', wagtail.core.blocks.ListBlock(wagtail.core.blocks.CharBlock(), required=False))])))]))])),
                ('banner_image', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='wagtailimages.Image')),
                ('icon', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='wagtailimages.Image')),
            ],
            options={
                'abstract': False,
            },
            bases=('home.basepage',),
        ),
        migrations.CreateModel(
            name='UsesRootPage',
            fields=[
                ('basepage_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='home.BasePage')),
                ('banner_image', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='wagtailimages.Image')),
            ],
            options={
                'abstract': False,
            },
            bases=('home.basepage',),
        ),
    ]

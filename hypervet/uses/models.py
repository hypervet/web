import json

from django.core import serializers
from django.db import models
from django.shortcuts import render
from wagtail.admin.edit_handlers import StreamFieldPanel
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel

from home.models import BasePage, BODY_FIELD


class UsesRootPage(BasePage):
    banner_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='+'
    )

    subpage_types = ['uses.UsesPage']

    content_panels = Page.content_panels + [
        ImageChooserPanel('banner_image'),
    ]

    def serve(self, request, *args, **kwargs):
        children = UsesPage.objects.live().child_of(self)
        context = self.get_context(request, *args, **kwargs)
        context['children'] = children
        return render(request, self.template, context)


class UsesPage(BasePage):
    icon = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='+'
    )
    banner_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='+'
    )
    body = BODY_FIELD

    parent_pages_types = ['uses.UsesRootPage', 'uses.UsesPage']

    content_panels = Page.content_panels + [
        ImageChooserPanel('banner_image'),
        ImageChooserPanel('icon'),
        StreamFieldPanel('body'),
    ]

from django import template
from wagtailtrans.models import TranslatablePage, TranslatableSiteRootPage

from .navigation_tags import language_site_root

register = template.Library()


@register.inclusion_tag('templatetags/inner_banner.html', takes_context=True)
def inner_banner(context, calling_page=None, background_image=None):
    if not calling_page:
        raise AssertionError('Missing calling_page')
    homepage = language_site_root(context)
    parent_pages = get_parent_pages(calling_page, homepage, found_parents=[])
    parent_pages.reverse()

    return {
        'request': context['request'],
        'homepage': homepage,
        'parent_pages': parent_pages,
        'page': calling_page,
        'background_image': background_image
    }


def get_parent_pages(start_page, end_page, found_parents=[]):
    """Traverse up the tree until certain page is reached."""
    try:
        parent_page = start_page.get_parent()
        if parent_page.content_type == end_page.content_type:
            return found_parents
        else:
            found_parents.append(parent_page)
            return get_parent_pages(parent_page, end_page, found_parents)
    except:
        return found_parents

from django import template
from django.conf import settings
from wagtailtrans.models import TranslatablePage, TranslatableSiteRootPage

register = template.Library()


def language_site_root(context):
    language = context['request'].LANGUAGE_CODE
    site_root = TranslatableSiteRootPage.objects.live().get(title='HYPERVET')
    language_root = TranslatablePage.objects.live().child_of(site_root)
    return language_root.filter(language__code=language).first()


@register.simple_tag(takes_context=True)
def get_site_root(context):
    return language_site_root(context)


@register.inclusion_tag('templatetags/site_nav.html', takes_context=True)
def site_nav(context, calling_page=None):
    root = language_site_root(context)
    menuitems = root.get_children().live().in_menu()
    for menuitem in menuitems:
        menuitem.dropdown = menuitem.get_children().live().in_menu().exists()

    return {
        'request': context['request'],
        'root': root,
        'menuitems': menuitems,
        'calling_page': calling_page}


@register.inclusion_tag('templatetags/sitemap_footer.html', takes_context=True)
def sitemap_footer(context):
    root = language_site_root(context)
    menuitems = root.get_children().live()
    return {
        'request': context['request'],
        'menuitems': menuitems
    }


@register.inclusion_tag('templatetags/nav_footer.html', takes_context=True)
def nav_footer(context, menu_obj):
    children = menu_obj.root_page.get_children().live().in_menu()
    return {
        'request': context['request'],
        'menu_obj': menu_obj,
        'children': children,
    }

from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

from .models import FooterMenu


class FooterMenuAdmin(ModelAdmin):
    model = FooterMenu
    menu_label = 'Footer Menus'
    menu_icon = 'horizontalrule'
    menu_order = 250
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ('title',)


modeladmin_register(FooterMenuAdmin)

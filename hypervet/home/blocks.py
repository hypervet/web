from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock


class SlideBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    description = blocks.CharBlock()
    image = ImageChooserBlock()
    page = blocks.PageChooserBlock()
    button_text = blocks.CharBlock()
    background_position = blocks.CharBlock(
        required=False, null=True, blank=True, max_length=128)

    class Meta:
        template = 'home/blocks/carousel.html'


class DescriptionBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    intro = blocks.CharBlock(required=False)
    text = blocks.RichTextBlock()
    image = ImageChooserBlock()
    image_position = blocks.ChoiceBlock(choices=[
        ('left', 'left'),
        ('right', 'right')
    ])
    page = blocks.PageChooserBlock(required=False)
    button_text = blocks.CharBlock()

    class Meta:
        template = 'home/blocks/description.html'


class CTABlock(blocks.StructBlock):
    title = blocks.CharBlock()
    page = blocks.PageChooserBlock()
    button_text = blocks.CharBlock()

    class Meta:
        template = 'home/blocks/cta.html'


class BlogPreviewBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    intro = blocks.RichTextBlock(required=False)
    tag = blocks.CharBlock(required=False, null=True, blank=True)
    button_text = blocks.CharBlock()

    class Meta:
        template = 'home/blocks/blog_preview.html'

    def get_context(self, value, parent_context=None):
        from blog.models import BlogPost  # NOQA
        context = super().get_context(value, parent_context=parent_context)
        parent_page = parent_context['page']
        posts = BlogPost.objects.filter(language=parent_page.language)
        if value['tag']:
            posts = posts.filter(tags__name__iexact=value['tag'])
        context['posts'] = posts.live().order_by('-first_published_at')[:3]
        context['blog_index'] = context['posts'][0].get_parent()
        return context


class Testimonial(blocks.StructBlock):
    text = blocks.CharBlock()
    author = blocks.CharBlock()
    workplace = blocks.CharBlock()


class TestimonialBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    intro = blocks.RichTextBlock(required=False)
    testimonials = blocks.ListBlock(Testimonial())

    class Meta:
        template = 'home/blocks/testimonial.html'


class Feature(blocks.StructBlock):
    feature_name = blocks.CharBlock()
    text = blocks.RichTextBlock(required=False)
    image = ImageChooserBlock()
    page = blocks.PageChooserBlock(required=False)
    button_text = blocks.CharBlock()
    specifications = blocks.ListBlock(blocks.CharBlock(), required=False)


class FeaturesBlock(blocks.StructBlock):
    super_title = blocks.CharBlock()
    title = blocks.CharBlock()
    intro = blocks.RichTextBlock(required=False)
    features = blocks.ListBlock(Feature())

    class Meta:
        template = 'home/blocks/features.html'
        

class FreeTextBlock(blocks.StructBlock):
    super_title = blocks.CharBlock(required=False)
    title = blocks.CharBlock(required=False)
    free_text = blocks.RichTextBlock()

    class Meta:
        template = 'home/blocks/free_text.html'


class BulletItem(blocks.StructBlock):
    icon = ImageChooserBlock()
    title = blocks.TextBlock()
    text = blocks.RichTextBlock(required=False)


class BulletIconBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    super_title = blocks.CharBlock(required=False)
    bullet_points = blocks.ListBlock(BulletItem())

    class Meta:
        template = 'home/blocks/bullet_icons_block.html'

from random import randint

from django.db import models
from wagtail.admin.edit_handlers import (
    FieldPanel, MultiFieldPanel, PageChooserPanel, StreamFieldPanel)
from wagtail.core import blocks
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtailtrans.models import TranslatablePage

from cta.forms import SubscribeForm

from .blocks import (
    BlogPreviewBlock, BulletIconBlock, CTABlock, DescriptionBlock,
    FeaturesBlock, FreeTextBlock, SlideBlock, TestimonialBlock)


BODY_FIELD = StreamField([
    ('description', DescriptionBlock()),
    ('cta', CTABlock()),
    ('recent_blog_posts', BlogPreviewBlock()),
    ('testimonials', TestimonialBlock()),
    ('features', FeaturesBlock()),
    ('free_text', FreeTextBlock()),
    ('bullet_icons_list', BulletIconBlock())
])


class FooterMenu(models.Model):
    title = models.CharField(max_length=75)
    root_page = models.ForeignKey(
        Page, on_delete=models.CASCADE, null=True, blank=True)

    panels = [
        FieldPanel('title'),
        PageChooserPanel('root_page'),
    ]


class BasePage(TranslatablePage):
    facebook_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='+')
    facebook_title = models.TextField(blank=True)
    facebook_description = models.TextField(blank=True)

    twitter_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='+')
    twitter_title = models.TextField(blank=True)
    twitter_description = models.TextField(blank=True)

    promote_panels = Page.promote_panels + [
        MultiFieldPanel([
            FieldPanel('facebook_title'),
            FieldPanel('facebook_description'),
            ImageChooserPanel('facebook_image')
        ], heading="Facebook sharing"),
        MultiFieldPanel([
            FieldPanel('twitter_title'),
            FieldPanel('twitter_description'),
            ImageChooserPanel('twitter_image')
        ], heading="Twitter sharing")
    ]

    def get_context(self, request):
        from cta.models import EndOfPageCTA
        context = super().get_context(request)
        filtered_cta = EndOfPageCTA.objects.filter(live=True)
        filtered_cta_count = filtered_cta.count()
        random_cta = filtered_cta[randint(0, filtered_cta_count - 1)]
        context['footer_menus'] = FooterMenu.objects.all()
        context['end_of_page_cta'] = random_cta
        context['footer_subscribe_form'] = SubscribeForm()
        return context


class HomePage(BasePage):
    carousel = StreamField([
        ('slide', blocks.ListBlock(
            SlideBlock(), template='home/blocks/carousel.html'))
    ], blank=True)
    body = BODY_FIELD

    content_panels = Page.content_panels + [
        StreamFieldPanel('carousel'),
        StreamFieldPanel('body')
    ]


class FreePage(BasePage):
    banner_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='+')
    body = BODY_FIELD

    content_panels = Page.content_panels + [
        ImageChooserPanel('banner_image'),
        StreamFieldPanel('body'),
    ]

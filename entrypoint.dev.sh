#!/bin/sh

# load db
#cat dump_09-05-2019_21_10_06.sql | docker-compose -f docker-compose.prod.yml exec -T db psql -U postgres

cd hypervet/
pipenv run ./manage.py migrate
pipenv run ./manage.py collectstatic -l --no-input
pipenv run ./manage.py runserver 0.0.0.0:8000
exec "$@"